#!/usr/bin/perl

use strict;
use warnings;
use utf8;

# Karel Burda, 2013
# Script takes string folder with resources file in Android format and transforms all XMLs located in there into the iOS format to stdout

my $num_args = $#ARGV + 1;
my $err = "Usage: converter.pl <android_resource_folder>\n";

# check args
if ($num_args != 1)
{
    die $err;
}

# resolve src directory
my $folder = $ARGV[0];

opendir(DIR, $folder) or die "Could not open '$folder'. Make sure that it's a valid folder.\n";
# because of some asian languages
binmode(STDOUT, ":utf8");

my @xml_files = grep(/\.xml$/, readdir DIR);

for (my $i = 0; $i <= $#xml_files; $i++)
{
    
    # add whatever files you want to ignore here
    if ($xml_files[$i] eq "strings_no_translate.xml")
    {
        next;
    }
    
    my $filename = $folder . "/" . $xml_files[$i];
    
 	if (open(my $fh, '<:encoding(UTF-8)', $filename))
	{
            print "\n/* $xml_files[$i] */\n";

	    # foreach all rows in the repsective file
            while (my $row = <$fh>)
		{
            	    chomp $row;
            
            	    if ($row =~ m/string name="(.+)">(.*)<\/string>/i)
		    {
                	my $key = $1;
                	my $value = $2;
                
                	# then we need to replace "$[sd]" with "$@"
                	$value =~ s/\$[sd]/\$\@/g;
                
                	$value =~ s/"//g; # remove the slashes
                	$value =~ s/\\([^n'])/\\"$1/g; # add the slashes
                
                	# final print to the STDOUT
                	print "\"$key\" = \"$value\";\n";
            	    }
        	}
        }
    	else
    	{
            warn "Could not open file '$filename' $!";
    	}	
}


closedir DIR;

